import win_condition

def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


#End game
def game_over(board, slot):
    print_board(board)
    print("GAME OVER", board[slot], "has won")
    exit()


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if win_condition.check_rows(board) != None:
        winner = win_condition.check_rows(board)
        game_over(board, winner)

    elif win_condition.check_columns(board) != None:
        winner = win_condition.check_columns(board)
        game_over(board, winner)

    elif win_condition.check_diagonal(board) != None:
        winner = win_condition.check_diagonal(board)
        game_over(board, winner)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
