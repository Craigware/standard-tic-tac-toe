#Check winner
def check_rows(board):
    i = 0
    while i < 8:
        if board[i] == board[i+1] and board[i+1] == board[i+2]:
            return i
        else:
            i += 3


def check_columns(board):
    i = 0
    while i < 3:
        if board[i] == board[i+3] and board[i+3] == board[i+6]:
            return i
        else:
            i += 1


def check_diagonal(board):
    if board[0] == board[4] and board[4] == board[8]:
        return 0
    elif board[2] == board[4] and board[4] == board[6]:
        return 2
